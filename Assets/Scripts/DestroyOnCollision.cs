using System.Collections;
using System.Collections.Generic;
using System.Net;
using UnityEngine;

public class DestroyOnCollision : MonoBehaviour
{
    public AudioSource collectsfx;
    public AudioSource destroysfx;
    public void OnCollisionEnter(Collision OBS)
    {
      
            if (OBS.collider.CompareTag("Buff"))
            {
                Destroy(OBS.gameObject);
            collectsfx.Play();
            }
           if (OBS.collider.CompareTag("Obstacle"))
            {   Destroy(OBS.gameObject);
                destroysfx.Play();
            }
       

    }

}

 

