using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Timer : MonoBehaviour
{
    
    public int MaxTime;
    public float currentTime;
    public Timer timer;
    public float timetowait;
    
    public Slider Slider;
    public Gradient gradient;
    public Image Fill;
    public AudioSource destroy;
    private bool hasPlayed;
    void Awake()
    {
        Setmaxtime(MaxTime);
        currentTime = MaxTime;
        Slider.value = MaxTime;
    }

  
    

    void Update()
    {
        if (currentTime > MaxTime) currentTime = MaxTime;
        if (Time.time >= timetowait)
        {
            if (Slider.value > 0)
            {
                TimerCountDown(Time.deltaTime);
            }
        }
        if (currentTime<=0 && !hasPlayed)
        {   
            destroy.Play();
            hasPlayed = true;
        }
    }



    void TimerCountDown(float time)
    {
        currentTime -= time;
        timer.SetTimer(currentTime);            
    }
    
    
    public void Setmaxtime(int time)
    {
        Slider.maxValue = time;
    }



    public void SetTimer(float time)
    {
        Slider.value = time;
        Fill.color = gradient.Evaluate(Slider.normalizedValue);
    }
}