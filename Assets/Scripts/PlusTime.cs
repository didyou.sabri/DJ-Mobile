using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlusTime : MonoBehaviour
{
    public Timer tm;
    private void OnCollisionEnter(Collision buff)
    {
        if (buff.collider.CompareTag("Buff"))
        {
            tm.currentTime += 10;
            Debug.Log("Buff");
        }
        if (buff.collider.CompareTag("Obstacle"))
        { tm.currentTime -= 2;}

    }
}
