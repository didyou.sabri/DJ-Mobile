using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovements : MonoBehaviour
{
    public Rigidbody rb;
    public BoxCollider bx;

    public float Movespeed = 5;
    public Touch Touch;
    float DirectX;


    private void Update()
    {
        if (Input.touchCount>0)
        {
            Touch=Input.GetTouch(0);
            if (Touch.phase==TouchPhase.Moved)
            {
                transform.position = new Vector3(transform.position.x-Touch.deltaPosition.x*Movespeed,transform.position.y,transform.position.z);
            }
        }
       // DirectX = -Input.GetAxisRaw("Horizontal");
    }

    private void FixedUpdate()
    {
       // rb.velocity = new Vector2(DirectX * Movespeed, rb.velocity.y);
    }
}
