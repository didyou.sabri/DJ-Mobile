using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticleSystemOnCollision : MonoBehaviour
{
  
       public ParticleSystem Explosion;

  
    public void OnCollisionEnter(Collision OBS)
    {
        if (OBS.collider.tag == "Obstacle")
        {
            Explosion.Play();
            Handheld.Vibrate();

        }
      

    }
}

