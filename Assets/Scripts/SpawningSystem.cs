using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SpawningSystem : MonoBehaviour
{
    public Transform[] spawnpoints;
    public GameObject obs;
    public GameObject buff;
    int[] impairNum = {7, 9, 11};
    public Timer tm;
    float timecincestart;
    public float timeToSpawn, timeToSpawn2, timeDelay;
    // Start is called before the first frame update

    private void FixedUpdate()
    {
        timecincestart = Time.time;

        if (timecincestart >= timeToSpawn)
        {
            SpawnObs();
            timeToSpawn = timecincestart + timeDelay;
        }



        if (timecincestart >= timeToSpawn2)
        {
            SpawnBuff();
            timeToSpawn2 = timecincestart + RndmImPair(impairNum);
        }
        if (tm.currentTime<=0)
        {
            gameObject.SetActive(false);
            Destroy(gameObject.transform.parent);
            SceneManager.LoadScene(2);
        }
    }

    void SpawnObs()
    {
        int random = Random.Range(0, spawnpoints.Length);

        for (int i = 0; i < spawnpoints.Length; i++)
        {
            if (random != i)
            {
                Instantiate(obs, spawnpoints[i].position, Quaternion.identity);
            }
        }
    }



    void SpawnBuff()
    {
        int random = Random.Range(0, spawnpoints.Length);

        for (int j = 0; j < spawnpoints.Length; j++)
        {
            if (random == j)
            {
                Instantiate(buff, new Vector3 (spawnpoints[j].position.x, spawnpoints[j].position.y, spawnpoints[j].position.z + 3.5f), Quaternion.identity);
            }
        }
    }



    public int RndmImPair(int[] tab)
    {
        return tab[Random.Range(0, tab.Length)];
    }
}
